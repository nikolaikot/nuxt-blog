export const state = () => ({
  token: true
})

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
  CLEAR_TOKEN(state) {
    state.token = null
  }
}

export const actions = {
  login({ commit, dispatch }, formData) {
    try {
      const { token } = this.$axios.$post('/api/auth/admin/login', formData)
      console.log('token', token)
      dispatch('setToken', token)
    } catch (e) {
      commit('SET_ERROR', e, { root: true })
      throw e
    }
  },
  setToken({ commit }, token) {
    commit('SET_TOKEN', token)
  },
  logout({ commit }) {
    commit('CLEAR_TOKEN')
  },
  // eslint-disable-next-line require-await
  async createUser({ commit }, formData) {
    // eslint-disable-next-line no-useless-catch
    try {
      console.log('createUser', formData)
    } catch (e) {
      throw e
    }
  }
}

export const getters = {
  isAuthenticated: (state) => Boolean(state.token)
}
